##########
Portfolio
##########

This is an opportunity to use, develop, and display some skills that I would normally not have in my day to day work, but that might be of interest of potential employers.  

Current Skills being shown
==========================


Docker  
	The site the visitor will be seeing is hosted Docker containers.
BitBucket
	The code for the site is version controlled using git and stored on BitBucket.
CodeIgniter
	The site was written using the CodeIgniter framework.
Bootstrap 4
	The UI for the site uses Bootstrap 4.