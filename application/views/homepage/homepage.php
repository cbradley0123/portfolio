
</head>
<body>
  <div class="container">

    <!-- Include the navbar and pass in the page value -->
    <?php $this->load->view('templates/navbar', ['page'=>'home']);?>
    <section class="jumbotron text-center">
      <div class="container">

        <!-- Some information about this page -->
        <h1 class="jumbotron-heading">My Portfolio</h1>
        <p class="lead text-muted">My name is Clayton Bradley.  I have created this simple site to display and discuss some skills that I have acquired that may not be reflected in my professional experience on my resume.</p>
      </div>
    </section>

    <!-- An accordion box that expands to display information about my newest endeavors.  Long but not very impressive code wise. -->
    <div class="accordion" id="portfolioAccordion">
      <div class="card">
        <div class="card-header" id="dockerHeading">
          <h5 class="mb-0">
            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#docker" aria-expanded="true" aria-controls="docker">
              Docker
            </button>
          </h5>
        </div>

        <div id="docker" class="collapse show" aria-labelledby="dockerHeading" data-parent="#portfolioAccordion">
          <div class="card-body">
            <p>I have learned to use the Docker platform.  This site is being hosted on a Raspberry Pi 2 that has Docker running in it.  The docker platform has three containers on it.  One production server, that you are viewing this on.  One development server where I get to make changes without bothering you with the confusion that that can create.  One MySQL server for database storage.</p><p>The first two have an Apache server with PHP installed.  I created the image that they run on via a Dockerfile.  Click <a href="<?php echo base_url();?>download/Dockerfile" target="_blank"> HERE </a> to download that Dockerfile.</p> <p>The second container has MySQL running on it.  It uses the stock image from the docker repository.</p><p>These three containers are linked via a local network and communicate that way.  They also use external volumes for their data so that it remains even if the containers themselves go down.  Click <a href="<?php echo base_url();?>download/network_output.txt" target="_blank"> HERE </a> to download the output of the 'docker network inspect isolated_nw' command.</p><p>Click <a href="<?php echo base_url();?>database"> HERE </a> to see an output from the database.</p>
          </div>
        </div>
      </div>
      <div class="card">
        <div class="card-header" id="bitbucketHeading">
          <h5 class="mb-0">
            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#bitbucket" aria-expanded="false" aria-controls="bitbucket">
              BitBucket
            </button>
          </h5>
        </div>
        <div id="bitbucket" class="collapse" aria-labelledby="bitbucketHeading" data-parent="#portfolioAccordion">
          <div class="card-body">
            <p>I use the BitBucket repository for code versioning as well as for backup purposes.  I am also using it to manage changes between my development and production servers.  The development server pushes changes, the production server pulls them.</p><p>  Most of my code is for personal use, but both this and one or two others are out there for the public to see and use.  Click <a href="https://bitbucket.org/cbradley0123" target="_blank"> HERE </a> for my BitBucket account.</p>
          </div>
        </div>
      </div>
      <div class="card">
        <div class="card-header" id="codeigniterHeading">
          <h5 class="mb-0">
            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#codeigniter" aria-expanded="false" aria-controls="codeigniter">
              CodeIgniter
            </button>
          </h5>
        </div>
        <div id="codeigniter" class="collapse" aria-labelledby="codeigniterHeading" data-parent="#portfolioAccordion">
          <div class="card-body">
            <p>This site was built using the CodeIgniter MVC framework.  While this is a simple page that may have been more easily built using straight HTML and CSS, that really isn't the purpose of learning something new.  To show my use of this, you can click <a href="<?php echo base_url();?>download/routes.txt" target="_blank"> HERE </a> to see my routes table or <a href="<?php echo base_url();?>download/database.txt" target="_blank"> HERE </a> to see my database config table.  Note that both of these have been changed to .txt files from their original .php to allow you to download them.  Conversely, you can download the entire project by going to the BitBucket link above and downloading or inspecting the repo.</p>
          </div>
        </div>
      </div>
      <div class="card">
        <div class="card-header" id="bootstrapHeading">
          <h5 class="mb-0"> 
            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#bootstrap" aria-expanded="false" aria-controls="bootstrap">
              Bootstrap 4
            </button>
          </h5>
        </div>
        <div id="bootstrap" class="collapse" aria-labelledby="bootstrapHeading" data-parent="#portfolioAccordion">
          <div class="card-body">
            <p>Bootstrap 4.  What else is there to say.  I have used Bootstrap 3 for years and it has always been perfectly fine for what we needed.  However, in the spirit of learning new things, I have decided to migrate to the new version.  As with every version change, it is almost exactly the same as the old version, except where it isn't.  So I am still learning the nuances, but it is mostly painless.</p>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- My contact information -->
  <?php $this->load->view('templates/contact_info');?>