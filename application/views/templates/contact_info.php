<footer class="pt-4 my-md-5 pt-md-5 border-top">
    <div class="row justify-content-around">
      <div class="col-2">
        <h5>Contact</h5>
        <ul class="list-unstyled text-small">
          <li><a class="text-muted" href="https://www.linkedin.com/in/claytonbradley/">LinkedIn Profile</a></li>
          <li><a class="text-muted" href="mailto:cbradley0123@gmail.com">Email cbradley0123@gmail.com</a></li>
          <li><span class='text-muted'>Phone (864) 237-5724</span></li>
        </ul>
      </div>
    </div>
  </div>
</footer>