<!-- Ye olde, simple navbar -->
<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
	<span class="navbar-brand d-flex align-items-center">
		
	<!-- Why a floppy?  Because it takes me back to college when we used to turn these projects in on floppies.  -->
    <i class="fa fa-floppy-o fa-6" aria-hidden="true" style='padding-right: 10px'></i>          
    <strong> Portfolio</strong>
  </span>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
	  <span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarCollapse">
	  <ul class="navbar-nav mr-auto">

	  	<!-- Determine which page is currently home -->
	    <li class="nav-item<?PHP if ($page == 'home'){echo ' active';} ?>">
	      <a class="nav-link" href="<?PHP echo base_url()?>">Home</a>
	    </li>
	    <li class="nav-item<?PHP if ($page == 'database') echo ' active'; ?>">
	      <a class="nav-link" href="<?PHP echo base_url()?>database">Database</a>
	    </li>
	  </ul>
	</div>
</nav>