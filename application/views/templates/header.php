<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,height=device-height,initial-scale=1.0"/>

    <meta name="description" content="A CodeIgniter header file template">
    <meta name="author" content="Clayton Bradley">

    <!-- Note that although the below SHOULD set a page title, if used with web redirects it will not. -->
    <title><?php echo $title; ?></title>

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" type="text/css" href="/application/third_party/bootstrap-4.1.3/css/bootstrap.min.css">

    <!-- Custom Fonts -->
    <link rel="stylesheet" type="text/css" href="/application/third_party/font-awesome-4.1.0/css/font-awesome.min.css">

    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="/application/views/templates/css/portfolio.css">

    <!-- Favicon -->
    <!-- Note that these will not show up when using a web-redirect with uri masking.  The masking was used to make the browser url look prettier at the expense of the favicon and the title not showing properly -->
    <link rel="icon" type="image/png" sizes="32x32" href="/application/views/templates/img/favicon-16x16.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/application/views/templates/img/favicon-32x32.png">
    <link rel="icon" href="//application/views/templates/favicon.ico?" type="image/x-icon">

    <!-- Although my preference is to put the JS at the footer, it was placed in the header due to specific JS files needing to be run after jquery, but that was not possible unless I was going to include all JS files in the footer every time. -->
    <script src="/application/third_party/jquery-3.3.1/jquery-3.3.1.min.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js" integrity="sha384-0s5Pv64cNZJieYFkXYOTId2HMA2Lfb6q2nAcx2n0RTLUnCAoTTsS0nKEO27XyKcY" crossorigin="anonymous"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js" integrity="sha384-ZoaMbDF+4LeFxg6WdScQ9nnR1QC2MIRxA1O9KWEXQwns1G8UNyIEZIQidzb0T1fo" crossorigin="anonymous"></script>
    <![endif]-->
