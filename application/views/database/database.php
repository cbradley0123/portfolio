<!-- DataTables Bootstrap CSS -->
<link rel="stylesheet" type="text/css" href="/application/third_party/css/dataTables.bootstrap4.min.css">



</head>
<body>
  <div class="container">

    <!-- Include the navbar and pass in the page value -->
    <?php $this->load->view('templates/navbar', ['page' => 'database']);?>
    <section class="jumbotron text-center">
      <div class="container">

        <!-- Some information about this page -->
        <h1 class="jumbotron-heading">Simple Database Data Pull</h1>
        <p class="lead text-muted">Below is a simple database pull showing the last 10 connection to this site.  The purpose is to show the ability to use the CodeIgniter model to retrieve data.</p><p class="lead text-muted">This query uses a specific select (instead of a SELECT *), a where clause, and an order by clause.  This was built using the CI Query Builder class and DataTables.</p>
      </div>
    </section>

    <!-- Building a table DataTables stype -->
    <table id="last10loads" class="table table-striped table-bordered" style="width:100%">
      <thead>
          <tr>
              <!-- Set the column headers -->
              <th>IP</th>
              <th>Browser</th>
              <th>Browser Version</th>
              <th>Platform</th>
              <th>Visit Date Time (GMT)</th>
          </tr>
      </thead>
      <tbody>
        <!-- Iterate over the records that were passed in via the controller and create a row with each -->
        <?php
          foreach ($records as $key => $value) {
            echo '<tr>';
            echo '<td>' . $value['ip'] . '</td>'; 
            echo '<td>' . $value['browser'] . '</td>'; 
            echo '<td>' . $value['browserVersion'] . '</td>'; 
            echo '<td>' . $value['platform'] . '</td>'; 
            echo '<td>' . $value['date_time'] . '</td>'; 
            echo '</tr>';
           } 
        ?>
      </tbody>
    </table>
  </div>

  <!-- My contact information -->
  <?php $this->load->view('templates/contact_info');?>

<!-- JS for DataTables -->
<script src="/application/third_party/js/jquery.dataTables.min.js"></script>

<!-- JS for compatibility between DataTables and Bootstrap -->
<script src="/application/third_party/js/dataTables.bootstrap4.min.js "></script>