<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Database_Controller extends CI_Controller {
	public function __construct(){
     	parent::__construct();
  	}

	public function index()
	{
		// This is the main page of the database controller.  It will display a table
		// of the the 10 most recent visits to this site.

		// Load the helper / library files
		$this->load->helper('url');
		$this->load->library('user_agent');

		// Decide which environment we're in
		if (base_url() == 'http://clayton-portfolio-dev.dynu.net/') {
			$environment = 'development';
		} else {
			$environment = 'production';
		}

		// Start the visitor model, which will pull back visitor data
		$this->load->model('Visitor_model');

		// Pull the records
		$returned['records'] = $this->Visitor_model->getData($environment);

		// Set page title
		$data['title'] = 'Database Page';
		
		// Build page using header -> file -> footer, sending data to header and file
		$this->load->view("templates/header", $data);
		$this->load->view('database/database', $returned);
		$this->load->view("templates/footer");
	}
}