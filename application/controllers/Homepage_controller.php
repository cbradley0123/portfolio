<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Homepage_Controller extends CI_Controller {
	public function __construct(){
     	parent::__construct();
  	}

	public function index()
	{
		// This is the main page of the homepage controller.  It will display static 
		// content about me and my projects.

		// Load the helper / library files
		$this->load->helper('url');
		$this->load->library('user_agent');

		// Retrieve and store the data about the current visitor.
		$data['ip'] = $this->input->ip_address();
		$data['browser'] = $this->agent->browser();
		$data['browserVersion'] = $this->agent->version();
		$data['platform'] = $this->agent->platform();
		if (base_url() == 'http://clayton-portfolio-dev.dynu.net/') {
			$data['environment'] = 'development';
		} else {
			$data['environment'] = 'production';
		}

		// Start the visitor model, which will store visitor data
		$this->load->model('Visitor_model');

		// Record the visitor's data
		$this->Visitor_model->writeData($data);
		$data = [];

		// Set the page title
		$data['title'] = 'Portfolio Page';
		
		// Build page using header -> file -> footer, sending data to header 
		$this->load->view("templates/header", $data);
		$this->load->view('homepage/homepage');
		$this->load->view("templates/footer");
	}
	
	public function download($fileName = NULL) {
		// This is the download page of the homepage controller.  It is used to facilitate
		// downloading of desired files.

		// Load the helper / library files
		$this->load->helper('url');   
		$this->load->helper('download');

		// Check to see that we have a FileName provided instead of default NULL
	    if ($fileName) {
	    	// Get full path to the FileName
		    $file = getcwd() . "/application/views/homepage/resources/" . $fileName;		     
		    // check file exists    
		    if (file_exists ( $file )) {
			     // get file content
			     $data = file_get_contents ( $file );
			     //force download
			     force_download ( $fileName, $data );
		    } else {
		    	 // Slightly helpful message
			     echo "File does not exist!";
		    } 
	   } else {
		     // Slightly helpful message
		     echo "No file requested!";

	   }
	}
}