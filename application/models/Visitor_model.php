<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Visitor_model extends CI_Model {
  // This is the model to interact with the visitor table of the database

  function getData($environment){
    // Retrieve data from the table
    // $environment is the name of the environment we're working with

    // Have to connect to the db first
    $this->load->database();

    // Creates the query of "SELECT 
    //                          ip, browser, browserVersion, platform, date_time
    //                       FROM visitors
    //                       WHERE
    //                          environment = <environment variable passed in>
    //                          AND browser IS NOT NULL
    //                       ORDER BY date_time DESC
    //                       LIMIT 10"
    $this->db->select('ip, browser, browserVersion, platform, date_time');
    $this->db->where(array('environment'=>$environment, 'browser !='=>''));
    $this->db->order_by('date_time', 'desc');
    $q = $this->db->get('visitors', 10);

    // Get the returned records
    $response = $q->result_array();

    // Return the records to the controller
    return $response;
  }

  function writeData($data) {
    // Writes data to the table
    // $data is an associative array passed in

    // Have to connect to the db first
  	$this->load->database();

    // Executes an insert command based on the data received from controller
  	$this->db->insert('visitors', $data);
  }

}